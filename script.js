var btn_send = document.querySelector(".send-text")
var chat = document.querySelector(".chat-messages")
var box_id

function cria_msg() {
    let input = document.querySelector(".input")
    if(input.value != ""){
        let mensagem = document.createElement("p")
        mensagem.innerText = input.value
        input.value = ""
        mensagem.classList.add("message-text")
        return mensagem
    }
}

function cria_botoes() {
    let buttons = document.createElement("div")
    let btn_edit = document.createElement("button")
    let btn_remove = document.createElement("button")
    btn_edit.classList.add("text-button")
    btn_edit.classList.add("btn_edit")
    btn_edit.innerText = "Editar"
    btn_remove.classList.add("text-button")
    btn_remove.classList.add("btn_remove")
    btn_remove.innerText = "Remover"
    buttons.style.display = "flex"
    buttons.style.justifyContent = "end"
    buttons.style.width = "100%"
    buttons.style.gap = "2px"
    buttons.append(btn_edit)
    buttons.append(btn_remove)
    return buttons
}

function enviar_mensagem() {
    let msg_box = document.createElement("div")
    msg_box.classList.add("msg-box")
    let msg = cria_msg()
    if(msg != undefined){
        msg_box.append(msg)
        msg_box.append(cria_botoes())
        msg_box.setAttribute("id", Date.now())
        chat.append(msg_box)
    }
}

function editar_mensagem() {
    let input = document.querySelector(".input")
    let msg_box = document.getElementById(box_id)
    let msg = msg_box.firstChild
    if(input.value == ""){
        chat.removeChild(msg_box)
    }else{
        msg.innerText = input.value
    }
    input.value = ""
    btn_send.removeEventListener("click", editar_mensagem)
    btn_send.addEventListener("click", enviar_mensagem)
    let btn_remove = msg_box.lastChild.lastChild
    btn_remove.disabled = false
    btn_send.innerText = "Enviar"
    btn_send.style.background = "#0066cc"
}

btn_send.addEventListener("click", enviar_mensagem)

document.addEventListener("click", function(e) {
    if(e.target && e.target.classList.contains("btn_remove")){
        let box_id = e.target.parentNode.parentNode.id
        let msg_box = document.getElementById(box_id)
        chat.removeChild(msg_box)
    }
})

document.addEventListener("click", function(e) {
    if(e.target && e.target.classList.contains("btn_edit")){
        let input = document.querySelector(".input")
        let btn_remove = e.target.nextSibling
        btn_remove.disabled = true
        box_id = e.target.parentNode.parentNode.id
        let msg_box = document.getElementById(box_id)
        let msg = msg_box.firstChild
        input.value = msg.innerText
        btn_send.removeEventListener("click", enviar_mensagem)
        btn_send.addEventListener("click", editar_mensagem)
        btn_send.innerText = "Editar"
        btn_send.style.background = "green"
    }
})
